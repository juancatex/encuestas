-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-12-2021 a las 05:19:26
-- Versión del servidor: 10.4.19-MariaDB
-- Versión de PHP: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto_emi`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carreras`
--

CREATE TABLE `carreras` (
  `id` int(11) NOT NULL,
  `iduniversidad` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `nivel` varchar(30) NOT NULL,
  `observacion` varchar(300) NOT NULL,
  `estado` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carreras`
--

INSERT INTO `carreras` (`id`, `iduniversidad`, `nombre`, `nivel`, `observacion`, `estado`, `created_at`, `updated_at`) VALUES
(1, 1, 'FISICA', '', 'FISICA NUCLEAR', 1, '2021-07-27 15:23:29', '2021-07-27 15:23:29'),
(2, 2, 'INGENIERIA PETROLERA', 'LICENCIATURA', 'NINGUNA', 1, '2021-07-27 16:01:07', '2021-07-27 16:01:07'),
(3, 2, 'MEDICINA', 'LICENCIATURA', 'NINGUNA', 1, '2021-07-27 16:14:13', '2021-07-27 16:14:13'),
(4, 1, 'BIOLOGIA', 'LICENCIATURA', 'NINGUNA', 1, '2021-07-27 16:22:31', '2021-07-27 16:22:31'),
(5, 1, 'ENFERMERIA', 'LICENCIATURA', 'PRUEBA', 1, '2021-07-27 16:24:51', '2021-08-11 23:37:51'),
(6, 3, 'TURISMO', 'LICENCIATURA', 'NINGUNO', 1, '2021-07-27 16:40:13', '2021-07-27 16:40:13'),
(7, 3, 'INGENIERIA CIVIL', 'LICENCIATURA', 'INSERT', 1, '2021-07-27 16:40:59', '2021-07-27 16:40:59'),
(8, 1, 'VETERINARIA ZOOTECNIA', 'DOCTORADO', 'NINGUNO AAAAAA', 1, '2021-07-27 16:42:40', '2021-08-23 15:19:33'),
(9, 6, 'ARQUICTECTURA', 'LICENCIATURA', 'NINGUNO', 1, '2021-07-29 01:15:27', '2021-07-29 01:15:27'),
(10, 5, 'INGENIERIA PETROLERA', 'LICENCIATURA', 'NINGUNO', 1, '2021-07-29 01:15:58', '2021-07-29 01:15:58'),
(11, 5, 'INGENIERIA PETROLERA', 'LICENCIATURA', 'NINGUNO', 1, '2021-07-29 01:15:59', '2021-07-29 01:15:59'),
(12, 4, 'MECATRONICA', 'LICENCIATURA', 'LICENCITURA', 1, '2021-08-11 16:21:33', '2021-08-11 16:21:33'),
(13, 4, 'ENFERMERIA', 'TECNICO SUPERIOR', 'DATO DE PRUEBA', 1, '2021-08-11 23:37:26', '2021-08-11 23:37:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emails`
--

CREATE TABLE `emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `idpromo` int(11) DEFAULT NULL,
  `encuesta` int(10) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `estado` int(1) NOT NULL DEFAULT 0,
  `date_reg` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `emails`
--

INSERT INTO `emails` (`id`, `idpromo`, `encuesta`, `code`, `estado`, `date_reg`, `date_update`) VALUES
(1, 1, 1, 'RLOQlNmr', 1, '2021-11-02 00:56:27', '2021-11-02 00:56:41'),
(2, 1, 2, 'SdcnvjPm', 1, '2021-11-02 01:34:06', '2021-11-02 01:39:59'),
(3, 1, 3, 'pxINn2KO', 1, '2021-11-02 01:38:06', '2021-11-02 01:39:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encuestas`
--

CREATE TABLE `encuestas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `iduser` int(11) DEFAULT NULL,
  `idusermod` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `encuestas`
--

INSERT INTO `encuestas` (`id`, `nombre`, `descripcion`, `estado`, `iduser`, `idusermod`, `created_at`, `updated_at`) VALUES
(1, 'dd1', 'dd1', 2, NULL, NULL, '2021-11-02 00:37:48', '2021-11-02 00:56:27'),
(2, 'dd2', 'dd2', 2, NULL, NULL, '2021-11-02 01:20:09', '2021-11-02 01:34:06'),
(3, 'dd3', 'dd3', 2, NULL, NULL, '2021-11-02 01:37:49', '2021-11-02 01:38:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encuesta_data`
--

CREATE TABLE `encuesta_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `idpreguntas` int(10) UNSIGNED NOT NULL,
  `respuesta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `iduser` int(11) DEFAULT NULL,
  `idusermod` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `encuesta_data`
--

INSERT INTO `encuesta_data` (`id`, `idpreguntas`, `respuesta`, `activo`, `iduser`, `idusermod`, `created_at`, `updated_at`) VALUES
(1, 1, 'as', 1, NULL, NULL, '2021-11-02 00:37:48', '2021-11-02 00:37:48'),
(2, 1, 's', 1, NULL, NULL, '2021-11-02 00:37:48', '2021-11-02 00:37:48'),
(3, 1, 'd', 1, NULL, NULL, '2021-11-02 00:37:48', '2021-11-02 00:37:48'),
(4, 1, 'd', 1, NULL, NULL, '2021-11-02 00:37:48', '2021-11-02 00:37:48'),
(5, 2, 's', 1, NULL, NULL, '2021-11-02 00:37:48', '2021-11-02 00:37:48'),
(6, 2, 's', 1, NULL, NULL, '2021-11-02 00:37:48', '2021-11-02 00:37:48'),
(7, 2, 's', 1, NULL, NULL, '2021-11-02 00:37:48', '2021-11-02 00:37:48'),
(8, 3, 'as', 1, NULL, NULL, '2021-11-02 01:20:09', '2021-11-02 01:20:09'),
(9, 3, 's', 1, NULL, NULL, '2021-11-02 01:20:09', '2021-11-02 01:20:09'),
(10, 3, 'd', 1, NULL, NULL, '2021-11-02 01:20:09', '2021-11-02 01:20:09'),
(11, 3, 'd', 1, NULL, NULL, '2021-11-02 01:20:09', '2021-11-02 01:20:09'),
(12, 4, 's', 1, NULL, NULL, '2021-11-02 01:20:09', '2021-11-02 01:20:09'),
(13, 4, 's', 1, NULL, NULL, '2021-11-02 01:20:09', '2021-11-02 01:20:09'),
(14, 4, 's', 1, NULL, NULL, '2021-11-02 01:20:09', '2021-11-02 01:20:09'),
(15, 5, 'as', 1, NULL, NULL, '2021-11-02 01:37:49', '2021-11-02 01:37:49'),
(16, 5, 's', 1, NULL, NULL, '2021-11-02 01:37:49', '2021-11-02 01:37:49'),
(17, 5, 'd1', 1, NULL, NULL, '2021-11-02 01:37:49', '2021-11-02 01:37:49'),
(18, 5, 'd2', 1, NULL, NULL, '2021-11-02 01:37:49', '2021-11-02 01:37:49'),
(19, 6, 's', 1, NULL, NULL, '2021-11-02 01:37:49', '2021-11-02 01:37:49'),
(20, 6, 's', 1, NULL, NULL, '2021-11-02 01:37:49', '2021-11-02 01:37:49'),
(21, 6, 's', 1, NULL, NULL, '2021-11-02 01:37:49', '2021-11-02 01:37:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encuesta_preguntas`
--

CREATE TABLE `encuesta_preguntas` (
  `id` int(10) UNSIGNED NOT NULL,
  `idencuesta` int(10) UNSIGNED NOT NULL,
  `pregunta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `iduser` int(11) DEFAULT NULL,
  `idusermod` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `encuesta_preguntas`
--

INSERT INTO `encuesta_preguntas` (`id`, `idencuesta`, `pregunta`, `activo`, `iduser`, `idusermod`, `created_at`, `updated_at`) VALUES
(1, 1, 'asdfasdf', 1, NULL, NULL, '2021-11-02 00:37:48', '2021-11-02 00:37:48'),
(2, 1, 'fffefe', 1, NULL, NULL, '2021-11-02 00:37:48', '2021-11-02 00:37:48'),
(3, 2, 'asdfasdf', 1, NULL, NULL, '2021-11-02 01:20:09', '2021-11-02 01:20:09'),
(4, 2, 'fffefe', 1, NULL, NULL, '2021-11-02 01:20:09', '2021-11-02 01:20:09'),
(5, 3, 'asdfasdf', 1, NULL, NULL, '2021-11-02 01:37:49', '2021-11-02 01:37:49'),
(6, 3, 'fffefe', 1, NULL, NULL, '2021-11-02 01:37:49', '2021-11-02 01:37:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo_menuses`
--

CREATE TABLE `grupo_menuses` (
  `idgrupo` int(10) UNSIGNED NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icono` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `grupo_menuses`
--

INSERT INTO `grupo_menuses` (`idgrupo`, `descripcion`, `icono`, `activo`, `created_at`, `updated_at`) VALUES
(1, 'Administración', 'ft-grid', 1, NULL, NULL),
(2, 'Encuesta', 'ft-check-square', 1, NULL, NULL),
(3, 'Unidad', 'ft-users', 1, NULL, NULL),
(4, 'Academico', 'ft-tag', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `institutos`
--

CREATE TABLE `institutos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `sigla` varchar(10) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `estado` int(11) NOT NULL,
  `observacion` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `institutos`
--

INSERT INTO `institutos` (`id`, `nombre`, `sigla`, `tipo`, `estado`, `observacion`, `created_at`, `updated_at`) VALUES
(1, 'UNIVERSIDAD MAYOR DE SAN ANDRES', 'UMSA', 'ACREDITADA', 1, 'UNIVERSIDAD ESTATAL PLENA', '2021-07-27 13:52:05', '2021-07-27 13:52:05'),
(2, 'UNIVERSIDAD PUBLICA DE EL ALTO', 'UPEA', 'ESTATAL', 1, 'UNIVERSIDAD PLENA', '2021-07-27 13:41:26', '2021-07-27 15:37:39'),
(3, 'UNIVERSIDAD DE AQUINO BOLIVIA', 'UDABOL', 'PRIVADA', 1, 'PRUEBA', '2021-07-27 16:39:38', '2021-07-27 16:39:38'),
(4, 'UNIVERSIDAD DE LOYOLA', 'LOYOLA', 'PRIVADA', 1, 'ALGO', '2021-07-27 16:42:09', '2021-07-27 16:42:09'),
(5, 'UNIVERSIDAD LA SALLE', 'LA SALLE', 'PRIVADA', 1, 'DATO DE PRUEBA', '2021-07-29 01:14:13', '2021-07-29 01:14:13'),
(6, 'UNIVERSIDAD FRANZ TAMAYO', 'UNIFRANZ', 'PRIVADA', 1, 'DATO DE PRUEBA', '2021-07-29 01:14:15', '2021-07-29 01:14:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_07_24_153135_create_categorias_table', 1),
(4, '2019_07_31_164422_create_articulos_table', 2),
(5, '2019_08_05_142944_create_personas_table', 3),
(6, '2019_08_05_192617_create_proveedores_table', 4),
(7, '2019_08_05_235406_create_roles_table', 5),
(8, '2019_08_06_000000_create_users_table', 6),
(9, '2019_08_10_100831_create_ingresos_table', 7),
(10, '2019_08_10_101104_create_detalle_ingresos_table', 7),
(11, '2019_08_23_114134_create_ventas_table', 8),
(12, '2019_08_23_114151_create_detalle_ventas_table', 8),
(13, '2019_09_01_192321_create_notifications_table', 9),
(15, '2021_08_29_143045_create_vues_table', 10),
(16, '2021_08_31_191401_create_grupo_menuses_table', 11),
(17, '2021_08_30_143045_create_vues_table', 12),
(18, '2021_10_28_110710_create_rol_vues_table', 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` int(10) UNSIGNED NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paralelos`
--

CREATE TABLE `paralelos` (
  `id` int(11) NOT NULL,
  `detalle` varchar(45) NOT NULL,
  `condicion` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `paralelos`
--

INSERT INTO `paralelos` (`id`, `detalle`, `condicion`, `created_at`, `updated_at`) VALUES
(1, 'ROJO', 1, '2021-10-05 01:54:30', '2021-10-05 01:54:30'),
(2, 'AZUL', 1, '2021-06-11 15:21:32', '2021-06-11 14:21:32'),
(3, 'AMARILLO', 1, '2021-10-05 01:59:38', '2021-10-05 01:59:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_estudios`
--

CREATE TABLE `personal_estudios` (
  `id` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL,
  `iduniversidad` int(11) NOT NULL,
  `idcarrera` int(11) NOT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `nivel` varchar(30) NOT NULL,
  `estado` int(11) NOT NULL,
  `observacion` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `personal_estudios`
--

INSERT INTO `personal_estudios` (`id`, `idpersonal`, `iduniversidad`, `idcarrera`, `fecha_ingreso`, `nivel`, `estado`, `observacion`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2021-08-04', 'LICENCIATURA', 1, 'DATOS DE PRUEBA', '2021-08-10 00:43:18', '2021-08-10 00:36:12'),
(2, 1, 1, 4, '2021-08-11', 'LICENCIATURA', 1, 'DATOS DE PRUEBA', '2021-08-12 00:22:33', '2021-08-12 00:22:33'),
(3, 8, 4, 12, '2020-01-01', 'LICENCIATURA', 1, 'NINGUNA', '2021-08-12 18:06:50', '2021-08-12 18:06:50'),
(4, 8, 6, 9, '2021-01-01', 'TECNICO MEDIO', 1, 'NINGUNA', '2021-08-14 22:49:15', '2021-08-14 22:49:15'),
(5, 2, 1, 4, '2021-01-01', 'DIPLOMADO', 1, 'NINGUNA', '2021-08-14 23:09:40', '2021-08-14 23:09:40'),
(6, 7, 3, 7, '2020-01-01', 'DIPLOMADO', 1, 'NINGUNA', '2021-08-15 01:14:18', '2021-08-15 01:14:18'),
(8, 13, 3, 6, '2021-08-03', 'TECNICO MEDIO', 1, 'NINGUNA', '2021-08-21 11:18:23', '2021-08-21 11:18:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_promociones`
--

CREATE TABLE `personal_promociones` (
  `id` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `idpromocion` int(11) NOT NULL,
  `idparalelo` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `observacion` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `personal_promociones`
--

INSERT INTO `personal_promociones` (`id`, `idpersona`, `idpromocion`, `idparalelo`, `estado`, `observacion`, `created_at`, `updated_at`) VALUES
(1, 103, 7, 3, 1, 'NINGUNA', '2021-11-02 00:55:39', '2021-11-02 00:55:39'),
(2, 107, 7, 2, 1, 'NINGUNA', '2021-12-16 03:45:40', '2021-12-16 03:45:50'),
(3, 106, 5, 3, 1, 'NINGUNA', '2021-12-16 03:46:00', '2021-12-16 03:46:00'),
(4, 105, 1, 3, 1, 'NINGUNA', '2021-12-21 03:10:02', '2021-12-21 03:10:02'),
(5, 104, 6, 2, 1, 'NINGUNA', '2021-12-21 03:10:19', '2021-12-21 03:10:19'),
(6, 1, 6, 3, 1, 'NINGUNA', '2021-12-21 04:06:35', '2021-12-21 04:06:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` int(10) NOT NULL,
  `nombre` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paterno` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `materno` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado_civil` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexo` enum('MASCULINO','FEMENINO','','') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_nac` date DEFAULT current_timestamp(),
  `tipo_documento` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_documento` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` int(11) DEFAULT 1,
  `observacion` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `nombre`, `paterno`, `materno`, `estado_civil`, `sexo`, `fecha_nac`, `tipo_documento`, `num_documento`, `direccion`, `telefono`, `email`, `estado`, `observacion`, `created_at`, `updated_at`) VALUES
(1, 'JIMENA', 'TARQUI', 'FLORES', 'SOLTERO', 'FEMENINO', '1992-02-01', 'CI', '6962844', 'EL ALTO', '71731240', 'ximenatf80@gmail.com', 1, 'NINGUNA', '2021-08-23 21:36:32', '2021-12-21 04:05:33'),
(103, '222222', '22', '22222', 'SOLTERO', 'FEMENINO', '2019-01-01', 'CI', '1231231', 'dddd', '7754953', 'juan_carlos1345@hotmail.com', 1, 'NINGUNA', '2021-11-02 00:55:39', '2021-11-02 00:55:39'),
(104, 'eeee', 'eeee', 'eeee', 'SOLTERO', 'FEMENINO', '2021-12-31', 'CI', '123123', 'por confirmar', '77748484', 'jua@hotmail.com', 1, 'NINGUNA', '2021-12-16 01:26:16', '2021-12-16 01:26:16'),
(105, 'eeee', 'eeee', 'eeee', 'SOLTERO', 'FEMENINO', '2021-12-31', 'CI', '123123', 'por confirmar', '77748484', 'jua@hotmail.com', 1, 'NINGUNA', '2021-12-16 01:27:15', '2021-12-16 01:27:15'),
(106, 'eeee', 'eeee', 'eeee', 'SOLTERO', 'FEMENINO', '2021-12-31', 'CI', '123123', 'por confirmar', '77748484', 'jua@hotmail.com', 1, 'NINGUNA', '2021-12-16 01:28:01', '2021-12-16 01:28:01'),
(107, 'juan Carlos', 'Colque', 'Quispe', 'SOLTERO', 'MASCULINO', '2021-12-31', 'CI', '8301452', 'por confirmar', '77549539', 'juan@hotmail.com', 1, 'NINGUNA', '2021-12-16 02:32:05', '2021-12-16 02:32:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promocions`
--

CREATE TABLE `promocions` (
  `id` int(11) NOT NULL,
  `anio` int(4) NOT NULL,
  `descripcion_promo` varchar(255) NOT NULL,
  `total` int(11) NOT NULL,
  `condicion` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `promocions`
--

INSERT INTO `promocions` (`id`, `anio`, `descripcion_promo`, `total`, `condicion`, `created_at`, `updated_at`) VALUES
(1, 2019, 'Entre Lágrimas Y Risas Siempre Estaremos Unidos.', 176, 1, '2021-08-23 16:25:21', '2021-08-23 15:25:21'),
(2, 2018, 'Estudiantes Sin Límites Orgullosamente Unidos.', 146, 1, '2021-08-23 16:25:55', '2021-08-23 15:25:55'),
(5, 2017, 'Generación Estudiantil con Nuevas Ideas, Aspiraciones y Logros.', 153, 1, '2021-08-23 16:26:40', '2021-08-23 15:26:40'),
(6, 2016, 'Somos La Imagen Del Éxito.', 78, 1, '2021-08-23 16:27:28', '2021-08-23 15:27:28'),
(7, 2020, 'Aunque Lejos Estemos Siempre Unidos Seremos.ewre', 350, 1, '2021-10-05 23:11:57', '2021-10-05 23:11:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

CREATE TABLE `respuestas` (
  `id` int(10) UNSIGNED NOT NULL,
  `idencuesta` int(10) UNSIGNED NOT NULL,
  `idpreguntas` int(10) UNSIGNED NOT NULL,
  `idrespuesta` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `respuestas`
--

INSERT INTO `respuestas` (`id`, `idencuesta`, `idpreguntas`, `idrespuesta`, `created_at`, `updated_at`) VALUES
(1, 3, 5, 18, '2021-11-02 00:56:41', '2021-11-02 00:56:41'),
(2, 3, 5, 18, '2021-11-02 00:56:41', '2021-11-02 00:56:41'),
(3, 3, 5, 16, '2021-11-02 01:39:35', '2021-11-02 01:39:35'),
(4, 3, 6, 19, '2021-11-02 01:39:35', '2021-11-02 01:39:35'),
(5, 2, 3, 9, '2021-11-02 01:39:59', '2021-11-02 01:39:59'),
(6, 2, 4, 13, '2021-11-02 01:39:59', '2021-11-02 01:39:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`, `descripcion`, `condicion`) VALUES
(1, 'Administrador', 'Encargado de la Administracion del Sistema', 1),
(2, 'Director3w', 'Director de la Unidad Educativa aUEFAB', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_vues`
--

CREATE TABLE `rol_vues` (
  `id` int(10) UNSIGNED NOT NULL,
  `idrol` int(10) UNSIGNED NOT NULL,
  `idvue` int(10) UNSIGNED NOT NULL,
  `permisos` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `rol_vues`
--

INSERT INTO `rol_vues` (`id`, `idrol`, `idvue`, `permisos`, `activo`, `created_at`, `updated_at`) VALUES
(3, 1, 3, '%7B%22Asignar%22:true,%22Activar%22:true,%22Desactivar%22:true%7D', 1, NULL, NULL),
(19, 1, 4, '%7B%7D', 1, '2021-11-04 04:19:16', '2021-12-16 02:26:35'),
(20, 1, 5, '%7B%22Envio_email%22:true%7D', 1, '2021-11-04 04:23:28', '2021-12-16 02:26:32'),
(21, 1, 6, '%7B%7D', 1, '2021-11-04 04:24:10', '2021-12-15 23:31:08'),
(22, 1, 1, '%7B%22Registrar%22:true,%22Activar%22:true,%22Desactivar%22:true,%22Modificar%22:true%7D', 1, '2021-12-15 23:30:13', '2021-12-15 23:30:13'),
(23, 1, 2, '%7B%22Registrar%22:true,%22Activar%22:true,%22Desactivar%22:true,%22Modificar%22:true%7D', 1, '2021-12-15 23:30:23', '2021-12-15 23:30:23'),
(27, 1, 10, '%7B%22Registrar%22:true,%22Activar%22:true,%22Desactivar%22:true,%22Modificar%22:true,%22Reporte_ver%22:true%7D', 1, '2021-12-15 23:30:43', '2021-12-15 23:30:43'),
(31, 1, 12, '%7B%22Registrar%22:true,%22Modificar%22:true,%22Reporte_ver%22:true,%22Desactivar%22:true,%22Activar%22:true%7D', 1, '2021-12-16 03:04:51', '2021-12-16 03:04:51'),
(32, 1, 11, '%7B%22Reporte_ver%22:true%7D', 1, '2021-12-16 03:05:50', '2021-12-16 03:05:50'),
(35, 1, 7, '%7B%22Registrar%22:true,%22Activar%22:true,%22Desactivar%22:true,%22Modificar%22:true,%22Reporte_ver%22:true%7D', 1, '2021-12-16 03:37:02', '2021-12-16 03:37:02'),
(36, 1, 8, '%7B%22Registrar%22:true,%22Activar%22:true,%22Desactivar%22:true,%22Modificar%22:true,%22Reporte_ver%22:true%7D', 1, '2021-12-16 03:37:07', '2021-12-16 03:37:07'),
(37, 1, 9, '%7B%22Registrar%22:true,%22Activar%22:true,%22Desactivar%22:true,%22Modificar%22:true,%22Reporte_ver%22:true%7D', 1, '2021-12-16 03:37:12', '2021-12-16 03:37:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `universidades`
--

CREATE TABLE `universidades` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `sigla` varchar(20) NOT NULL,
  `carreras` varchar(50) NOT NULL,
  `estado` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `universidades`
--

INSERT INTO `universidades` (`id`, `descripcion`, `sigla`, `carreras`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'UNIVERSIDAD MAYOR DE SAN ANDRES', 'UMSA', 'INGENIERIA CIVIL', 1, '2021-07-26 02:39:14', '2021-07-26 02:39:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `usuario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT 1,
  `idrol` int(10) UNSIGNED NOT NULL,
  `logo` int(11) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `usuario`, `password`, `condicion`, `idrol`, `logo`, `remember_token`) VALUES
(1, 'tarqui', '$2y$10$1uM0vStZ84wP7dAru4WYeuidPK3S3HYn9/Cnjvu4LPWLOiDpcsgdG', 1, 1, 8, '5CzcHsExP6WjIkiToASGk3IyefHIPjTXqMa2uC9e6Ye1KgKJeJ3kenD4n0MZ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vues`
--

CREATE TABLE `vues` (
  `idvue` int(10) UNSIGNED NOT NULL,
  `idgrupo` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `orden` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vues`
--

INSERT INTO `vues` (`idvue`, `idgrupo`, `nombre`, `template`, `activo`, `orden`, `created_at`, `updated_at`) VALUES
(1, 1, 'Usuarios', 'users', 1, 1, NULL, NULL),
(2, 1, 'Rol', 'rol', 1, 2, NULL, NULL),
(3, 1, 'Asignación de Vistas', 'rolvues', 1, 3, NULL, NULL),
(4, 2, 'Encuesta', 'vueencuesta', 1, 1, NULL, NULL),
(5, 2, 'Envio de encuesta', 'envioemail', 1, 2, NULL, NULL),
(6, 2, 'Resultados', 'resultados', 1, 3, NULL, NULL),
(7, 3, 'Estudiante', 'estudiante', 1, 1, NULL, NULL),
(8, 3, 'Paralelo', 'paralelos', 1, 2, NULL, NULL),
(9, 3, 'Promocion', 'promocion', 1, 3, NULL, NULL),
(10, 4, 'Estudios', 'estudios', 1, 1, NULL, NULL),
(11, 4, 'Unidad Academica', 'unidadacademica', 1, 2, NULL, NULL),
(12, 4, 'Carreras', 'carreras', 1, 3, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carreras`
--
ALTER TABLE `carreras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iduniversidad` (`iduniversidad`);

--
-- Indices de la tabla `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `encuestas`
--
ALTER TABLE `encuestas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `encuesta_data`
--
ALTER TABLE `encuesta_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `encuesta_data_idpreguntas_foreign` (`idpreguntas`);

--
-- Indices de la tabla `encuesta_preguntas`
--
ALTER TABLE `encuesta_preguntas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `encuesta_preguntas_idencuesta_foreign` (`idencuesta`);

--
-- Indices de la tabla `grupo_menuses`
--
ALTER TABLE `grupo_menuses`
  ADD PRIMARY KEY (`idgrupo`);

--
-- Indices de la tabla `institutos`
--
ALTER TABLE `institutos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`);

--
-- Indices de la tabla `paralelos`
--
ALTER TABLE `paralelos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `personal_estudios`
--
ALTER TABLE `personal_estudios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpersonal` (`idpersonal`),
  ADD KEY `iduniversidad` (`iduniversidad`),
  ADD KEY `idcarrera` (`idcarrera`);

--
-- Indices de la tabla `personal_promociones`
--
ALTER TABLE `personal_promociones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpromocion` (`idpromocion`),
  ADD KEY `idparalelo` (`idparalelo`),
  ADD KEY `idpersona` (`idpersona`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `promocions`
--
ALTER TABLE `promocions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `respuestas_idencuesta_foreign` (`idencuesta`),
  ADD KEY `respuestas_idpreguntas_foreign` (`idpreguntas`),
  ADD KEY `respuestas_idrespuesta_foreign` (`idrespuesta`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_nombre_unique` (`nombre`);

--
-- Indices de la tabla `rol_vues`
--
ALTER TABLE `rol_vues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rol_vues_idrol_foreign` (`idrol`),
  ADD KEY `rol_vues_idvue_foreign` (`idvue`);

--
-- Indices de la tabla `universidades`
--
ALTER TABLE `universidades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `users_usuario_unique` (`usuario`),
  ADD KEY `users_id_foreign` (`id`),
  ADD KEY `users_idrol_foreign` (`idrol`);

--
-- Indices de la tabla `vues`
--
ALTER TABLE `vues`
  ADD PRIMARY KEY (`idvue`),
  ADD KEY `vues_idgrupo_foreign` (`idgrupo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carreras`
--
ALTER TABLE `carreras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `encuestas`
--
ALTER TABLE `encuestas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `encuesta_data`
--
ALTER TABLE `encuesta_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `encuesta_preguntas`
--
ALTER TABLE `encuesta_preguntas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `grupo_menuses`
--
ALTER TABLE `grupo_menuses`
  MODIFY `idgrupo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `institutos`
--
ALTER TABLE `institutos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `paralelos`
--
ALTER TABLE `paralelos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `personal_estudios`
--
ALTER TABLE `personal_estudios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `personal_promociones`
--
ALTER TABLE `personal_promociones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT de la tabla `promocions`
--
ALTER TABLE `promocions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `rol_vues`
--
ALTER TABLE `rol_vues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `universidades`
--
ALTER TABLE `universidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `vues`
--
ALTER TABLE `vues`
  MODIFY `idvue` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carreras`
--
ALTER TABLE `carreras`
  ADD CONSTRAINT `carreras_ibfk_1` FOREIGN KEY (`iduniversidad`) REFERENCES `institutos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `encuesta_data`
--
ALTER TABLE `encuesta_data`
  ADD CONSTRAINT `encuesta_data_idpreguntas_foreign` FOREIGN KEY (`idpreguntas`) REFERENCES `encuesta_preguntas` (`id`);

--
-- Filtros para la tabla `encuesta_preguntas`
--
ALTER TABLE `encuesta_preguntas`
  ADD CONSTRAINT `encuesta_preguntas_idencuesta_foreign` FOREIGN KEY (`idencuesta`) REFERENCES `encuestas` (`id`);

--
-- Filtros para la tabla `personal_estudios`
--
ALTER TABLE `personal_estudios`
  ADD CONSTRAINT `personal_estudios_ibfk_1` FOREIGN KEY (`iduniversidad`) REFERENCES `institutos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `personal_estudios_ibfk_2` FOREIGN KEY (`idcarrera`) REFERENCES `carreras` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `personal_estudios_ibfk_3` FOREIGN KEY (`idpersonal`) REFERENCES `personas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personal_promociones`
--
ALTER TABLE `personal_promociones`
  ADD CONSTRAINT `personal_promociones_ibfk_1` FOREIGN KEY (`idparalelo`) REFERENCES `paralelos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `personal_promociones_ibfk_2` FOREIGN KEY (`idpromocion`) REFERENCES `promocions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `personal_promociones_ibfk_3` FOREIGN KEY (`idpersona`) REFERENCES `personas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `respuestas`
--
ALTER TABLE `respuestas`
  ADD CONSTRAINT `respuestas_idencuesta_foreign` FOREIGN KEY (`idencuesta`) REFERENCES `encuestas` (`id`),
  ADD CONSTRAINT `respuestas_idpreguntas_foreign` FOREIGN KEY (`idpreguntas`) REFERENCES `encuesta_preguntas` (`id`),
  ADD CONSTRAINT `respuestas_idrespuesta_foreign` FOREIGN KEY (`idrespuesta`) REFERENCES `encuesta_data` (`id`);

--
-- Filtros para la tabla `rol_vues`
--
ALTER TABLE `rol_vues`
  ADD CONSTRAINT `rol_vues_idrol_foreign` FOREIGN KEY (`idrol`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `rol_vues_idvue_foreign` FOREIGN KEY (`idvue`) REFERENCES `vues` (`idvue`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_idrol_foreign` FOREIGN KEY (`idrol`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `vues`
--
ALTER TABLE `vues`
  ADD CONSTRAINT `vues_idgrupo_foreign` FOREIGN KEY (`idgrupo`) REFERENCES `grupo_menuses` (`idgrupo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
