<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class GrupoMenus extends Model
{
    use HasFactory;
    protected $primaryKey = 'idgrupo';
    protected $table = 'grupo_menuses';
    protected $fillable = ['idgrupo','descripcion','icono','activo'];
    public function vues(){
        return $this->hasMany(Vues::class,'idgrupo')->where('activo','1')->orderBy('orden', 'asc');
    }
    public function vuess(){
        return $this->hasMany(Vues::class,'idgrupo')->where('activo','1')->whereIn('idvue', function ($query) {
            $query->select('idvue')->from('rol_vues')->where('activo','1')->where('idrol', '=',Auth::user()->idrol);
           })->orderBy('orden', 'asc');
    }
}
