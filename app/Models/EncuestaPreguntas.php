<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EncuestaPreguntas extends Model
{
    use HasFactory;
    protected $table = 'encuesta_preguntas';
    protected $fillable = ['id','idencuesta','pregunta','activo','iduser','idusermod'];
    public function data(){
        return $this->belongsTo(Encuesta::class);
    }
    public function respuestas(){
        return $this->hasMany(EncuestaData::class,'idpreguntas')->where('activo','1');
    }
}
