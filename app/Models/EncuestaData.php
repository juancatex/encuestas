<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EncuestaData extends Model
{
    use HasFactory;
    protected $table = 'encuesta_data';
    protected $fillable = ['id','idpreguntas','respuesta','activo','iduser','idusermod'];
    public function data(){
        return $this->belongsTo(EncuestaPreguntas::class);
    }
}
