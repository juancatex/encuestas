<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instituto extends Model
{
    use HasFactory;
    protected $table = 'institutos';
    protected $fillable = ['nombre','sigla','tipo','estado','observacion']; 
}
