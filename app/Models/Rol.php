<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    use HasFactory;
    const CREATED_AT = null;
    const UPDATED_AT = null;
    protected $table = 'roles';
    protected $fillable = ['nombre','descripcion','condicion']; 

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

}
