<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Personal_Promociones extends Model
{
    use HasFactory;
    protected $table = 'personal_promociones';
    protected $fillable = ['id','idpersona','idpromocion','idparalelo','estado','observacion'];
}
