<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vues extends Model
{
    use HasFactory;
    protected $primaryKey = 'idvue';
    protected $table = 'vues';
    protected $fillable = ['idvue','idgrupo','nombre','template','activo','orden'];
    public function data(){
        return $this->belongsTo(GrupoMenus::class);
    }
}
