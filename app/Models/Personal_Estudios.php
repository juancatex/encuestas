<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Personal_Estudios extends Model
{
    use HasFactory;
    protected $table = 'personal_estudios';
    protected $fillable = ['id','idpersonal','iduniversidad','idcarrera','estado','nivel','fecha_ingreso','observacion'];
}
