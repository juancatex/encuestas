<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Emails extends Model
{
    use HasFactory;
    protected $table = 'emails';
    const CREATED_AT = 'date_reg';
const UPDATED_AT = 'date_update';
    protected $fillable = ['idpromo','code','date_reg','date_update','id','encuesta']; 
}
