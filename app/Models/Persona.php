<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;
    protected $fillable = ['nombre','paterno','materno','estado_civil','sexo','fecha_nac','tipo_documento','num_documento','direccion','telefono','email'];
 
}
