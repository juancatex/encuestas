<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RolVues extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $table = 'rol_vues';
    protected $fillable = ['id','idrol','idvue','permisos']; 
}
