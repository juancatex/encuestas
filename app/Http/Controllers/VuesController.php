<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vues;

class VuesController extends Controller
{
    public function getvues(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $rol=$request->id;
        $vues = Vues::join('grupo_menuses','vues.idgrupo','grupo_menuses.idgrupo')
        ->where('vues.activo', '=', '1') 
        ->whereNotIn('vues.idvue', function ($query) use ($rol) { 
            // $query->select('idvue')->from('rol_vues')->where('activo','1')->where('idrol', '=',$rol);
            $query->select('idvue')->from('rol_vues')->where('idrol', '=',$rol);
        })
        ->select('vues.idvue','vues.template','vues.nombre','grupo_menuses.descripcion')
        ->orderBy('vues.idgrupo', 'asc')
        ->orderBy('vues.orden', 'asc')
        ->get();

        
        return ['vues' => $vues];
    }
}
