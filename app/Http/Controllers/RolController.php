<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rol;

class RolController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;
        
        if ($buscar==''){
            $roles = Rol::orderBy('id', 'desc')->paginate(5);
        }
        else{
            $roles = Rol::where($criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(5);
        }
        

        return [
            'pagination' => [
                'total'        => $roles->total(),
                'current_page' => $roles->currentPage(),
                'per_page'     => $roles->perPage(),
                'last_page'    => $roles->lastPage(),
                'from'         => $roles->firstItem(),
                'to'           => $roles->lastItem(),
            ],
            'roles' => $roles
        ];
    }
    public function rolasig(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
           $roles = Rol::where('condicion','1')->orderBy('id', 'desc')->get(); 
        return [ 
            'roles' => $roles
        ];
    }
    public function selectRol(Request $request)
    {
        $roles = Rol::where('condicion', '=', '1')
        ->select('id','nombre')
        ->orderBy('nombre', 'asc')->get();

        return ['roles' => $roles];
    } 
    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
 
            $rolvue = Rol::findOrFail($request->id); 
            $rolvue->nombre = $request->nombre; 
            $rolvue->descripcion = $request->descripcion;
            $rolvue->save(); 
           
    }
    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
  
            $rolvue = new Rol();
            $rolvue->nombre = $request->nombre; 
            $rolvue->descripcion = $request->descripcion;
            $rolvue->save(); 

         
    }
    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = Rol::findOrFail($request->id);
        $user->condicion = '0';
        $user->save();
    }
    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = Rol::findOrFail($request->id);
        $user->condicion = '1';
        $user->save();
    }
}
