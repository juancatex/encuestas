<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\RolVues;

class RolVuesController extends Controller
{
    public function getvuesrol(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $roles = RolVues::join('vues','rol_vues.idvue','vues.idvue')
        ->join('grupo_menuses','vues.idgrupo','grupo_menuses.idgrupo')
        ->where('rol_vues.idrol', '=', $request->id) 
        ->select('rol_vues.id','vues.nombre','grupo_menuses.descripcion','rol_vues.activo')
        ->orderBy('rol_vues.id', 'asc')->get();

        return ['roles' => $roles];
    }
    public function regrolvue(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
  
            $rolvue = new RolVues();
            $rolvue->idrol = $request->rol; 
            $rolvue->idvue = $request->vue;
            $rolvue->permisos = $request->perm;
            $rolvue->save();  
    }
    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        // $user = RolVues::findOrFail($request->id);
        // $user->activo = '0';
        // $user->save();
        RolVues::where('id',$request->id)->delete();
    }
    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = RolVues::findOrFail($request->id);
        $user->activo = '1';
        $user->save();
        
    }
}
