<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Persona;
use Exception;


class UserController extends Controller
{
    public function index(Request $request)
    {
       // if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;
        
        if ($buscar==''){
            $personas = User::join('personas','users.id','=','personas.id')
            ->join('roles','users.idrol','=','roles.id')
            ->select('personas.id','personas.nombre','personas.paterno','personas.materno','personas.estado_civil',
            'personas.sexo','personas.fecha_nac','personas.tipo_documento','personas.num_documento','personas.observacion',
            'personas.direccion','personas.telefono','personas.email','users.usuario','users.password',
            'users.condicion','users.idrol','roles.nombre as rol')
            ->where('users.condicion','=',1)
            ->orderBy('personas.id', 'desc')->paginate(5);
        }
        else{
            $personas = User::join('personas','users.id','=','personas.id')
            ->join('roles','users.idrol','=','roles.id')
            ->select('personas.id','personas.nombre','personas.paterno','personas.materno','personas.estado_civil',
            'personas.sexo','personas.fecha_nac','personas.tipo_documento','personas.num_documento','personas.observacion',
            'personas.direccion','personas.telefono','personas.email','users.usuario','users.password',
            'users.condicion','users.idrol','roles.nombre as rol')
            ->where('personas.'.$criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(5);
        }
        
        return [
            'pagination' => [
                'total'        => $personas->total(),
                'current_page' => $personas->currentPage(),
                'per_page'     => $personas->perPage(),
                'last_page'    => $personas->lastPage(),
                'from'         => $personas->firstItem(),
                'to'           => $personas->lastItem(),
            ],
            'personas' => $personas
        ];
    }
    public function getuserupdate(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
 
            $personas = User::join('personas','users.id','=','personas.id')
            ->join('roles','users.idrol','=','roles.id')
            ->select('personas.id','personas.nombre','personas.paterno','personas.materno','personas.estado_civil',
            'personas.sexo','personas.fecha_nac','personas.tipo_documento','personas.num_documento','personas.observacion',
            'personas.direccion','personas.telefono','personas.email','users.usuario','users.password','users.logo',
            'users.condicion','users.idrol','roles.nombre as rol')
            ->where('users.condicion','=',1)
            ->where('users.id','=',Auth::user()->id)->first(); 
        
        return [ 
            'personas' => $personas
        ];
    }
    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
 
            $persona = new Persona();
            $persona->nombre = $request->nombre;
            $persona->paterno = $request->paterno;
            $persona->materno = $request->materno;
            $persona->estado_civil = $request->estado_civil;
            $persona->sexo = $request->sexo;
            $persona->fecha_nac = $request->fecha_nac;
            $persona->tipo_documento = 'CI';
            $persona->num_documento = $request->num_documento;
            $persona->direccion = $request->direccion;
            $persona->telefono = $request->telefono;
            $persona->email = $request->email;
            $persona->estado = '1';
            $persona->observacion = $request->observacion;
            $persona->save();

            $user = new User();
            $user->id = $persona->id;
            $user->idrol = $request->idrol;
            $user->usuario = $request->usuario;
            $user->password = bcrypt( $request->password);
            $user->condicion = '1';            
            $user->save();

         
    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();

            $user = User::findOrFail($request->id);
            $persona = Persona::findOrFail($user->id);
            $persona->nombre = $request->nombre;
            $persona->paterno = $request->paterno;
            $persona->materno = $request->materno;
            $persona->estado_civil = $request->estado_civil;
            $persona->sexo = $request->sexo;
            $persona->fecha_nac = $request->fecha_nac;
            $persona->tipo_documento = 'CI';
            $persona->num_documento = $request->num_documento;
            $persona->direccion = $request->direccion;
            $persona->telefono = $request->telefono;
            $persona->email = $request->email; 
            $persona->estado = '1';
            $persona->observacion = $request->observacion;
            $persona->save();

            
            $user->usuario = $request->usuario;
            $user->password = bcrypt( $request->password);
            $user->condicion = '1';
            $user->idrol = $request->idrol;
            $user->logo = $request->logo;
            $user->save();

            DB::commit();
        } catch (Exception $e){
            DB::rollBack();
        }
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = User::findOrFail($request->id);
        $user->condicion = '0';
        $user->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = User::findOrFail($request->id);
        $user->condicion = '1';
        $user->save();
    }
}
