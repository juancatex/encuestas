<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Emails;

class EncuestaDataController extends Controller
{
    public function registro (Request $request){
        if (!$request->ajax()) return redirect('/');
        $promocion = new Emails();
        $promocion->idpreguntas = $request->id;
        $promocion->respuesta = $request->res; 
        $promocion->save();
        
    } 
}
