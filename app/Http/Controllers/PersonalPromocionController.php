<?php

namespace App\Http\Controllers;

use App\Models\Personal_Promociones;
use App\Models\Persona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Exception;

class PersonalPromocionController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;
        
        // if ($buscar==''){
        //     $personasPromocion =Persona::join('personal_promociones','personas.id','=','personal_promociones.idpersona')
        //     ->join('paralelos','personal_promociones.idparalelo','paralelos.id')
        //     ->join('promocions','personal_promociones.idpromocion','promocions.id')
        //     ->select('personas.id','personas.nombre','personas.paterno','personas.materno','personas.num_documento','personas.estado_civil','personas.fecha_nac','personas.sexo',
        //     'personas.email','personas.telefono','personas.direccion','personas.estado as estadouser','personal_promociones.id as estudios_id',
        //     'personal_promociones.idpersona','personal_promociones.idpromocion','personal_promociones.idparalelo',
        //     'personal_promociones.estado','paralelos.detalle','promocions.anio','promocions.descripcion_promo')
        //     ->orderBy('personas.id', 'desc')->paginate(10);
            
        // }
        // else{
        //     $personasPromocion = Persona::join('personal_promociones','personas.id','=','personal_promociones.idpersona')
        //     ->join('paralelos','personal_promociones.idparalelo','paralelos.id')
        //     ->join('promocions','personal_promociones.idpromocion','promocions.id')
        //     ->select('personas.id','personas.nombre','personas.paterno','personas.materno','personas.num_documento','personas.estado_civil','personas.fecha_nac','personas.sexo',
        //     'personas.email','personas.telefono','personas.direccion','personas.estado as estadouser','personal_promociones.id as estudios_id',
        //     'personal_promociones.idpersona','personal_promociones.idpromocion','personal_promociones.idparalelo',
        //     'personal_promociones.estado','paralelos.detalle','promocions.anio','promocions.descripcion_promo')
        //      ->where('personas.'.$criterio, 'like', '%'. $buscar . '%')->orderBy('personas.id', 'desc')->paginate(10);
        // }
        
        if ($buscar==''){
            $personasPromocion =Persona::join('personal_estudios','personas.id','=','personal_estudios.idpersonal')
            ->join('institutos','personal_estudios.iduniversidad','institutos.id') 
            ->join('carreras','personal_estudios.idcarrera','carreras.id') 
            ->select('personas.id','personas.nombre','personas.paterno','personas.materno','personas.num_documento', 
            'personas.email','personas.telefono', 'institutos.nombre as nombreu',
            'carreras.nombre as nombrec')
            ->where('personal_estudios.estado', 1)
            ->orderBy('personas.id', 'asc')->paginate(10);
            
        }
        else{
            $personasPromocion =Persona::join('personal_estudios','personas.id','=','personal_estudios.idpersonal')
            ->join('institutos','personal_estudios.iduniversidad','institutos.id') 
            ->join('carreras','personal_estudios.idcarrera','carreras.id') 
            ->select('personas.id','personas.nombre','personas.paterno','personas.materno','personas.num_documento', 
            'personas.email','personas.telefono', 'institutos.nombre as nombreu',
            'carreras.nombre as nombrec')
            ->where('personal_estudios.estado', 1)
            ->where('personas.'.$criterio, 'like', '%'. $buscar . '%')->orderBy('personas.id', 'desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total'        => $personasPromocion->total(),
                'current_page' => $personasPromocion->currentPage(),
                'per_page'     => $personasPromocion->perPage(),
                'last_page'    => $personasPromocion->lastPage(),
                'from'         => $personasPromocion->firstItem(),
                'to'           => $personasPromocion->lastItem(),
            ],
            'personasPrommocion' => $personasPromocion
        ];
    }  
    public function index2(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;
        
        if ($buscar==''){
            $personasPromocion =Persona::leftjoin('personal_promociones','personas.id','=','personal_promociones.idpersona')
            ->leftjoin('paralelos','personal_promociones.idparalelo','paralelos.id')
            ->leftjoin('promocions','personal_promociones.idpromocion','promocions.id')
            ->select('personas.id','personas.nombre','personas.paterno','personas.materno','personas.num_documento','personas.estado_civil','personas.fecha_nac','personas.sexo',
            'personas.email','personas.telefono','personas.direccion','personas.estado as estadouser','personal_promociones.id as estudios_id',
            'personal_promociones.idpersona','personal_promociones.idpromocion','personal_promociones.idparalelo',
            'personal_promociones.estado','paralelos.detalle','promocions.anio','promocions.descripcion_promo')
            ->orderBy('personas.id', 'desc')->paginate(10);
            
        }
        else{
            $personasPromocion = Persona::leftjoin('personal_promociones','personas.id','=','personal_promociones.idpersona')
            ->leftjoin('paralelos','personal_promociones.idparalelo','paralelos.id')
            ->leftjoin('promocions','personal_promociones.idpromocion','promocions.id')
            ->select('personas.id','personas.nombre','personas.paterno','personas.materno','personas.num_documento','personas.estado_civil','personas.fecha_nac','personas.sexo',
            'personas.email','personas.telefono','personas.direccion','personas.estado as estadouser','personal_promociones.id as estudios_id',
            'personal_promociones.idpersona','personal_promociones.idpromocion','personal_promociones.idparalelo',
            'personal_promociones.estado','paralelos.detalle','promocions.anio','promocions.descripcion_promo')
             ->where('personas.'.$criterio, 'like', '%'. $buscar . '%')->orderBy('personas.id', 'desc')->paginate(10);
        }
         

        return [
            'pagination' => [
                'total'        => $personasPromocion->total(),
                'current_page' => $personasPromocion->currentPage(),
                'per_page'     => $personasPromocion->perPage(),
                'last_page'    => $personasPromocion->lastPage(),
                'from'         => $personasPromocion->firstItem(),
                'to'           => $personasPromocion->lastItem(),
            ],
            'personasPrommocion' => $personasPromocion
        ];
    }  

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();
            $persona = new Persona();
            $persona->nombre = $request->nombre;
            $persona->paterno = $request->paterno;
            $persona->materno = $request->materno;
            $persona->estado_civil = $request->estado_civil;
            $persona->sexo = $request->sexo;
            $persona->fecha_nac = $request->fecha_nac;
            $persona->tipo_documento = 'CI';
            $persona->num_documento = $request->num_documento;
            $persona->direccion = $request->direccion;
            $persona->telefono = $request->telefono;
            $persona->email = $request->email;
            $persona->estado = '1';
            $persona->observacion = "NINGUNA";
            $persona->save();

            $personal_promocion = new Personal_Promociones();
            $personal_promocion->idpersona = $persona->id;
            $personal_promocion->idpromocion = $request->idpromocion;
            $personal_promocion->idparalelo = $request->idparalelo;
            $personal_promocion->estado = '1';   
            $personal_promocion->observacion = 'NINGUNA';            
            $personal_promocion->save();

            DB::commit();
        } catch (Exception $e){
            DB::rollBack();
        }
    }

public function registrarexcel (Request $request){
        if (!$request->ajax()) return redirect('/'); 
       
        try{
            DB::beginTransaction(); 
                for($i = 0; $i < count($request->data); ++$i) {  
                    if(is_numeric($request->data[$i][1])&&$request->data[$i][1]>0&&$request->data[$i][2]!=null&&$request->data[$i][3]!=null&&$request->data[$i][4]!=null
                    &&$request->data[$i][7]!=null&&$request->data[$i][8]!=null&&$request->data[$i][9]!=null){  
                        $persona = new Persona();
                        $persona->nombre = $request->data[$i][2];
                        $persona->paterno = $request->data[$i][3];
                        $persona->materno = $request->data[$i][4];
                        $persona->estado_civil ='SOLTERO';
                        $persona->sexo = $request->data[$i][6]==1?'MASCULINO':'FEMENINO';
                        $persona->fecha_nac = $request->data[$i][5];
                        $persona->tipo_documento = 'CI';
                        $persona->num_documento = $request->data[$i][7];
                        $persona->direccion ='por confirmar';
                        $persona->telefono = $request->data[$i][8];
                        $persona->email = $request->data[$i][9];
                        $persona->estado = '1';
                        $persona->observacion = "NINGUNA";
                        $persona->save();
            
                        // $personal_promocion = new Personal_Promociones();
                        // $personal_promocion->idpersona = $persona->id;
                        // $personal_promocion->idpromocion = $request->idpromocion;
                        // $personal_promocion->idparalelo = $request->idparalelo;
                        // $personal_promocion->estado = '1';   
                        // $personal_promocion->observacion = 'NINGUNA';            
                        // $personal_promocion->save();
                      
                    }
                 } 
            DB::commit();
        } catch (Exception $e){
            echo 'con eror'.$e;
            DB::rollBack();
        } 
    }
    public function update(Request $request){
        if (!$request->ajax()) return redirect('/');
       // return response()->json($request);
        $persona = Persona::findOrFail($request->id);
        $persona->nombre = $request->nombre;
        $persona->paterno = $request->paterno;
        $persona->materno = $request->materno;
        $persona->estado_civil = $request->estado_civil;
        $persona->sexo = $request->sexo;
        $persona->fecha_nac = $request->fecha_nac;
        $persona->tipo_documento = 'CI';
        $persona->num_documento = $request->num_documento;
        $persona->direccion = $request->direccion;
        $persona->telefono = $request->telefono;
        $persona->email = $request->email;
        $persona->estado = '1';
        $persona->observacion = "NINGUNA";
        $persona->save();
 
        
      

      
if (Personal_Promociones::where('idpersona', $request->id)->first()) {
    Personal_Promociones::where('idpersona', $request->id) 
    ->update(['idpromocion' =>$request->idpromocion, 
    'idparalelo' =>$request->idparalelo]);
} else {
    $personal_promocion = new Personal_Promociones();
    $personal_promocion->idpersona = $request->id;
    $personal_promocion->idpromocion = $request->idpromocion;
    $personal_promocion->idparalelo = $request->idparalelo;
    $personal_promocion->estado = '1';   
    $personal_promocion->observacion = 'NINGUNA';            
    $personal_promocion->save();
}
 
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = Persona::findOrFail($request->id);
        $user->estado = '0';
        $user->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = Persona::findOrFail($request->id);
        $user->estado = '1';
        $user->save();
    }

    public function carreraReporte(Request $request)
    {
        //$per_codigo = $request->per_codigo;

        $carreras = DB::table('carreras')
        ->join('institutos', 'carreras.iduniversidad','institutos.id')
        ->select('carreras.id',
                'carreras.nombre',
                'carreras.nivel',
                'carreras.observacion',
                'institutos.nombre as instituto',
                )
        ->orderBy('id','desc')
        ->get();

        /*$qr = QrCode::format('png')->size(100)->margin(0)->merge('../public/assets/img/qr.png')->generate("NRO: $numDoc\n NOMBRE: $datosQr\n FECHA: $date\n ANTECEDENTE $casoQr");
        $codigo = $qr;*/
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fecha_emision = date('d')." de ".$meses[date('n')-1]." del ".date('Y');
        $qr = QrCode::encoding('UTF-8')->size(100)->generate("FECHA: $fecha_emision");
        $codigo = $qr;

        $pdf = PDF::loadView('reportes.repcarreras',['carreras'=>$carreras,
                                                        'qr'=>$codigo])
        ->setPaper('letter', 'portrait');                                               
        
        return $pdf->stream('reporte.pdf');

    }
}
