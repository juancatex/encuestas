<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Respuestas;
use App\Models\Emails;
use App\Models\Personal_Estudios;
use App\Models\Carrera;

class RespuestasController extends Controller
{
    public function registro (Request $request){
        if (!$request->ajax()) return redirect('/');
        foreach ($request->data as  $value) { 
        $promocion = new Respuestas();
        $promocion->idencuesta = $request->encu;
        $promocion->idpreguntas = $value['pre']; 
        $promocion->idrespuesta = $value['res']; 
        $promocion->save();
       } 
       $emailsend=Emails::where('encuesta','=',$request->encu)->where('code','=',$request->code)->get(); 
       foreach ($emailsend as  $emalis){
        $emailsss = Emails::findOrFail($emalis->id);
        $emailsss->estado = 1; 
        $emailsss->save();
       }

       if (strlen($request->carrera)>0) { 
        Personal_Estudios::where('idpersonal',$request->user)->update(['estado' => 0]); 
        $carrera = Carrera::where('id',$request->carrera)->first(); 

        $estudios = new Personal_Estudios();
        $estudios->idpersonal = $request->user;
        $estudios->idcarrera =$carrera->id; 
        $estudios->iduniversidad =$carrera->iduniversidad; 
        $estudios->nivel =$carrera->nivel; 
        $estudios->estado =1; 
        $estudios->observacion ='NINGUNA'; 
        $estudios->save();
       }
        
    }
    public function getencuestaresultado (Request $request){
        if (!$request->ajax()) return redirect('/');
       return Respuestas::where('idencuesta','=',$request->id)->get();
    }
}
