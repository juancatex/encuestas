<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Encuesta;
use App\Models\EncuestaData;
use App\Models\Respuestas;
use App\Models\Emails;
use App\Models\EncuestaPreguntas;
use Illuminate\Support\Facades\DB; 
use Barryvdh\DomPDF\Facade as PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Mail;
use Carbon\Carbon;
use QuickChart;

class EncuestaController extends Controller
{
    public function lista(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;
        
        if ($buscar==''){
            $personasPromocion = Encuesta::where('estado','!=',0)->orderBy('id', 'desc')->paginate(10); 
        }
        else{
            $personasPromocion = Encuesta::where('estado','!=',0)->where($criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'asc')->paginate(10);
        }
        
        foreach($personasPromocion as $d){
            $d->preguntas; 
            foreach($d->preguntas as $d){
                $d->respuestas; 
            }
        }
        return [
            'pagination' => [
                'total'        => $personasPromocion->total(),
                'current_page' => $personasPromocion->currentPage(),
                'per_page'     => $personasPromocion->perPage(),
                'last_page'    => $personasPromocion->lastPage(),
                'from'         => $personasPromocion->firstItem(),
                'to'           => $personasPromocion->lastItem(),
            ],
            'listaencuesta' => $personasPromocion
        ];
    }  
    public function selectPromocion(Request $request){
        if (!$request->ajax()) return redirect('/');
        $promociones = DB::table('promocions')->where('condicion','=','1')
        ->select('id','anio','descripcion_promo')->orderBy('anio', 'asc')->get();
        return ['promociones' => $promociones];
    }
    public function registro (Request $request){
        if (!$request->ajax()) return redirect('/');
        $promocion = new Encuesta();
        $promocion->nombre = $request->nombre;
        $promocion->descripcion = $request->descrip; 
        $promocion->save();
        
    }
    public function registrarexcel (Request $request){
        if (!$request->ajax()) return redirect('/'); 
        try{
            DB::beginTransaction();
            if($request->data[4][2]!=null&&$request->data[5][2]!=null){
                $encuesta = new Encuesta();
                $encuesta->nombre = $request->data[4][2];
                $encuesta->descripcion = $request->data[5][2]; 
                $encuesta->save();
                for($i = 0; $i < count($request->data); ++$i) {  
                    if(is_numeric($request->data[$i][0])&&$request->data[$i][0]>0&&$request->data[$i][1]!=null){ 
                    $preguntas = new EncuestaPreguntas();
                    $preguntas->idencuesta = $encuesta->id;
                    $preguntas->pregunta = $request->data[$i][1]; 
                    $preguntas->save();
                 
                     for($r = 2; $r < count($request->data[$i]); ++$r) {  
                        if($request->data[$i][$r]!=null){ 
                            $respuestas = new EncuestaData();
                            $respuestas->idpreguntas = $preguntas->id;
                            $respuestas->respuesta = $request->data[$i][$r]; 
                            $respuestas->save();
                        }
                      }
                    }
                 }
            }
            DB::commit();
        } catch (Exception $e){
            DB::rollBack();
        } 
    }
    public function updata (Request $request){
        if (!$request->ajax()) return redirect('/');
        $promocion = Encuesta::findOrFail($request->id);
        $promocion->nombre = $request->nombre;
        $promocion->descripcion = $request->descrip; 
        $promocion->save(); 
    }
    public function delete (Request $request){
        if (!$request->ajax()) return redirect('/');
        $promocion = Encuesta::findOrFail($request->id); 
        $promocion->estado =0; 
        $promocion->save(); 
    }
    function randomname($length=8){
        return substr(str_shuffle("qwertyuioABCDpasdfghEFGHIjklzxcJKLMvbnm12345NOPQ6789RSTUVWXYZ"),0,$length);
    }
    public function sendemail(Request $request){
        if (!$request->ajax()) return redirect('/');

        $promociones = DB::table('personal_promociones')
        ->join('personas','personal_promociones.idpersona','personas.id')
        ->where('personal_promociones.idpromocion','=',$request->idpromo)
        ->select('personas.email','personal_promociones.id'
        ,'personas.nombre'
        ,'personas.id as iduser'
        ,'personas.paterno'
        ,'personas.materno'
        ,'personas.num_documento') ->get();
        $currentTime = Carbon::now();
 
            foreach ($promociones as  $value) { 
                $code=$this->randomname(); 
                $emails=$value->email;
                $name=$request->nombre;
                Mail::send('emailview', ['code'=>$code,'id' => $request->idencuesta,'user' => $value,'subject' => $request->nombre,'des' => $request->des,'fecha' => $currentTime->toDateTimeString()], 
                    function($message) use ($emails,$name)
                    {   $message->to($emails)->subject($name);    });

                $promocion = new Emails();
                $promocion->idpromo = $value->id;
                $promocion->encuesta = $request->idencuesta;
                $promocion->code = $code; 
                $promocion->save();
            }
               
        $encuesta = Encuesta::findOrFail($request->idencuesta);
        $encuesta->estado = 2; 
        $encuesta->save();
       
    }
    public function sendemail2(Request $request){
        if (!$request->ajax()) return redirect('/');
ini_set( 'display_errors', 1 );
error_reporting( E_ALL );
        $promociones = DB::table('personal_promociones')
        ->join('personas','personal_promociones.idpersona','personas.id')
        ->where('personal_promociones.idpromocion','=',$request->idpromo)
        ->select('email') ->get();
            $emails=array();
            foreach ($promociones as  $value) {
               // array_push($emails, $value->email);
                $from = "uefab@eufab.website";
    $to =$value->email;
    $subject = "UEFAB";
    $message = '
    <!DOCTYPE html>
<html>
<head>
    <title>Encuesta</title>
</head>
<body>  
    <p> Se le invita a ser parte de la siguiente encuesta:</p>
     
<p><a href="https://eufab.website/encuesta?id='.$request->idencuesta.'">'.$request->nombre.'</a></p>
</body>
</html>
    ';
    // The content-type header must be set when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers = "From:" . $from;
    if(mail($to,$subject,$message, $headers)) {
    //echo "Message was sent.";
    } else {
    //  echo "Message was not sent.";
    }
            }
         /*   $name=$request->nombre;
         
        Mail::send('email', ['id' => $request->idencuesta,'subject' => $request->nombre], function($message) use ($emails,$name)
        {    
            $message->to($emails)->subject($name);    
        });*/

        $encuesta = Encuesta::findOrFail($request->idencuesta);
        $encuesta->estado = 2; 
        $encuesta->save();
       
    }
    public function openencuesta(Request $request){
        return view('encuesta.encuestamain',['iden' =>$request->i,'code' =>$request->c,'user' =>$request->u]);
    }
    public function getencuesta(Request $request)
    {
        if (!$request->ajax()) return redirect('/'); 
       
        $emailsend=Emails::where('encuesta','=',$request->id)->where('code','=',$request->code)->limit(1)->get(); 
        
        if(count($emailsend)>0){ 
            if($emailsend[0]->estado==0){
               $personasPromocion = Encuesta::where('id','=',$request->id)->get();  
                foreach($personasPromocion as $d){
                    $d->preguntas; 
                    foreach($d->preguntas as $d){
                        $d->respuestas; 
                    }
                }
                return [ 
                    'encu' => $personasPromocion ,'men' => 1
                ];
            }else{
                return [ 
                    'encu' => null,'men' => 2
                ];
            }
        }else{
            return [ 
                'encu' => null,'men' => 3
            ];
        }
        
    }
    function bm_random_rgb() { 
        return 'rgb(' . rand(0, 255) . ',' . rand(0, 255) . ',' . rand(0, 255) . ')'; 
       }
    public function encuestasReporte(Request $request)
    {
       $encuesta= Encuesta::where('estado','=',2)->where('id','=',$request->idencuesta)->get()[0]; 
 
       $valuesrespuestas= Respuestas::where('idencuesta','=',$request->idencuesta)->get();
       foreach ($encuesta->preguntas as  $pregunta) {  
            $pregunta->count=0;
            foreach ($pregunta->respuestas as  $respuestas) { 
                $respuestas->count=0;

                foreach ($valuesrespuestas as  $intoresp) { 
                    if($intoresp['idencuesta'] == $encuesta['id'] && $intoresp['idpreguntas'] == $pregunta['id']&& $intoresp['idrespuesta'] ==  $respuestas['id']){
                        $respuestas->count++;
                        $pregunta->count++; 
                    }
                    
                } 

            } 
        
        }  
        
        $puntero=0;
        $encuestaarray = array();
        foreach ($encuesta->preguntas as  $pregunta) {  
            $titulo='';
            $color='';
            $valor='';
            $totalres=0;
            foreach ($pregunta["respuestas"] as  $respuestas) { 
                $titulo.='"'.$respuestas["respuesta"].'",';
                $color.="'".$this->bm_random_rgb()."',";
                $valor.=$respuestas["count"].","; 
                $totalres+=$respuestas["count"];
            } 
             
            $dataimage='{type: "horizontalBar",  data: { labels: ['.$titulo.'],datasets: [ { data: ['.$valor.']}]}, options: {legend: { display: false }}}';
           
            $chart = new QuickChart(array(
                'width' => 500,
                'height' => 300
              ));
              
              $chart->setConfig('{
                "type": "outlabeledPie",
                "data": {
                  "labels": ['.$titulo.'],
                  "datasets": [{
                      "backgroundColor": ['.$color.'],
                      "data": ['.$valor.']
                  }]
                },
                "options": {
                  "plugins": {
                    "legend": false,
                    "outlabels": {
                      "text": "%l %p",
                      "color": "white",
                      "stretch": 35,
                      "font": {
                        "resizable": true,
                        "minSize": 12,
                        "maxSize": 18
                      }
                    }
                  }
                }
              }');


           
            array_push($encuestaarray, array(
                "pregunta" => $pregunta["pregunta"], 
                "total" => $totalres, 
                "img" =>$chart->getShortUrl(),
            ));
        }   
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fecha_emision = date('d')." de ".$meses[date('n')-1]." del ".date('Y');
        $qr = QrCode::encoding('UTF-8')->size(100)->generate("FECHA: $fecha_emision");
        $codigo = $qr;

        $pdf = PDF::loadView('reportes.enviados',['data'=>$encuestaarray,'qr'=>$codigo,'til'=>$encuesta])
        ->setPaper('letter', 'portrait');      
        return $pdf->stream('reporte.pdf'); 
    }
    public function encuestasReportemain(Request $request)
    {
       $encuesta= Encuesta::where('estado','=',2)->where('id','=',$request->idencuesta)->get()[0]; 
 
       $valuesrespuestas= Respuestas::where('idencuesta','=',$request->idencuesta)->get();
       foreach ($encuesta->preguntas as  $pregunta) {  
            $pregunta->count=0;
            foreach ($pregunta->respuestas as  $respuestas) { 
                $respuestas->count=0;

                foreach ($valuesrespuestas as  $intoresp) { 
                    if($intoresp['idencuesta'] == $encuesta['id'] && $intoresp['idpreguntas'] == $pregunta['id']&& $intoresp['idrespuesta'] ==  $respuestas['id']){
                        $respuestas->count++;
                        $pregunta->count++; 
                    }
                    
                } 

            } 
        
        }  
        
        $puntero=0;
        $encuestaarray = array();
        foreach ($encuesta->preguntas as  $pregunta) {  
            $titulo='';
            $color='';
            $valor='';
            $totalres=0;
            foreach ($pregunta["respuestas"] as  $respuestas) { 
                $titulo.='"'.$respuestas["respuesta"].'",';
                $color.="'".$this->bm_random_rgb()."',";
                $valor.=$respuestas["count"].","; 
                $totalres+=$respuestas["count"];
            } 
             
            $dataimage='{type: "horizontalBar",  data: { labels: ['.$titulo.'],datasets: [ { data: ['.$valor.']}]}, options: {legend: { display: false }}}';
           
            $chart = new QuickChart(array(
                'width' => 500,
                'height' => 300
              ));
              
              $chart->setConfig('{
                "type": "outlabeledPie",
                "data": {
                  "labels": ['.$titulo.'],
                  "datasets": [{
                      "backgroundColor": ['.$color.'],
                      "data": ['.$valor.']
                  }]
                },
                "options": {
                  "plugins": {
                    "legend": false,
                    "outlabels": {
                      "text": "%l %p",
                      "color": "white",
                      "stretch": 35,
                      "font": {
                        "resizable": true,
                        "minSize": 12,
                        "maxSize": 18
                      }
                    }
                  }
                }
              }');


           
            array_push($encuestaarray, array(
                "pregunta" => $pregunta["pregunta"], 
                "total" => $totalres, 
                "img" =>$chart->getShortUrl(),
            ));
        }   
          
        return ['gm'=>$encuestaarray]; 
    }
    public function listaresultados(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $buscar = $request->buscar;
        $criterio = $request->criterio;
        
        if ($buscar==''){
            $personasPromocion = Encuesta::where('estado','=',2)->orderBy('id', 'desc')->paginate(10); 
        }
        else{
            $personasPromocion = Encuesta::where('estado','=',2)->where($criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'asc')->paginate(10);
        }
        
        foreach($personasPromocion as $d){
            $d->preguntas; 
            foreach($d->preguntas as $d){
                $d->respuestas; 
            }
        }
        return [
            'pagination' => [
                'total'        => $personasPromocion->total(),
                'current_page' => $personasPromocion->currentPage(),
                'per_page'     => $personasPromocion->perPage(),
                'last_page'    => $personasPromocion->lastPage(),
                'from'         => $personasPromocion->firstItem(),
                'to'           => $personasPromocion->lastItem(),
            ],
            'listaencuesta' => $personasPromocion
        ];
     
         
    }
}
