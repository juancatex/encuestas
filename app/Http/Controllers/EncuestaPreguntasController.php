<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EncuestaPreguntas;

class EncuestaPreguntasController extends Controller
{
    public function registro (Request $request){
        if (!$request->ajax()) return redirect('/');
        $promocion = new EncuestaPreguntas();
        $promocion->idencuesta = $request->id;
        $promocion->pregunta = $request->pre; 
        $promocion->save();
        
    }
    public function updata (Request $request){
        if (!$request->ajax()) return redirect('/');
        $promocion =  EncuestaPreguntas::findOrFail($request->id); 
        $promocion->pregunta = $request->pre; 
        $promocion->save();
        
    }
    public function delete (Request $request){
        if (!$request->ajax()) return redirect('/');
        $promocion =  EncuestaPreguntas::findOrFail($request->id); 
        $promocion->activo = 0; 
        $promocion->save();
        
    }
}
