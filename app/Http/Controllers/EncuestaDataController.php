<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EncuestaData;

class EncuestaDataController extends Controller
{
    public function registro (Request $request){
        if (!$request->ajax()) return redirect('/');
        $promocion = new EncuestaData();
        $promocion->idpreguntas = $request->id;
        $promocion->respuesta = $request->res; 
        $promocion->save();
        
    }
    
    public function updata (Request $request){
        if (!$request->ajax()) return redirect('/');
        $promocion = EncuestaData::findOrFail($request->id); 
        $promocion->respuesta = $request->res; 
        $promocion->save();
        
    }
    public function delete (Request $request){
        if (!$request->ajax()) return redirect('/');
        $promocion = EncuestaData::findOrFail($request->id); 
        $promocion->activo = 0; 
        $promocion->save();
        
    }
}
