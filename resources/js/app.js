 

// require('./bootstrap'); 
window._ = require('lodash');
window.$ = window.jQuery = require('jquery');
window.axios = require('axios');


window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.axios.interceptors.response.use(function (response) {
    return response
  }, function (error) { 
    if (error.response.status === 401) {
        app.nologin();
    }
    return Promise.reject(error)
  })

window.Vue = require('vue').default; 
Vue.use(require('vue-moment'));
import moment from 'moment'; 
Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY hh:mm')
  }
});
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
 
Vue.component('asignacionvista', require('./components/AsignacionDeVistas.vue').default);
Vue.component('vueencuesta', require('./components/Encuesta.vue').default);
Vue.component('envioemail', require('./components/EnvioEmail.vue').default);
Vue.component('formulario', require('./components/formulario.vue').default);
Vue.component('resultados', require('./components/EncuestaResultados.vue').default);
Vue.component('estudiante', require('./components/Estudiante.vue').default);
Vue.component('paralelos', require('./components/Paralelo.vue').default);
Vue.component('promocion', require('./components/Promocion.vue').default);
Vue.component('estudios', require('./components/Estudios.vue').default);
Vue.component('unidadacademica', require('./components/UnidadAcademica.vue').default);
Vue.component('carreras', require('./components/Carreras.vue').default);
Vue.component('users', require('./components/User.vue').default);
Vue.component('rol', require('./components/Rol.vue').default);
Vue.component('rolvues', require('./components/Rolvues.vue').default);
Vue.component('modaluser', require('./components/modaluser.vue').default);
Vue.component('home', require('./components/home.vue').default);
 
window.app = new Vue({
    el: '#app',
    data :{
        menu : 'home' ,
        perm : {},
        edith : 0 ,
        working : false,
        notifications : []
    },
    methods: { 
        validate(permisos){ 
            return JSON.parse(decodeURI(permisos));
         },
        getpermisos(nombre){
            var salida={};
            switch (nombre) { 
                    case 'users':
                    salida=require('./components/User.vue').default.data().arrayPermisos;
                    break; 
                    case 'rol':
                    salida=require('./components/Rol.vue').default.data().arrayPermisos;
                    break; 
                    case 'rolvues':
                    salida=require('./components/Rolvues.vue').default.data().arrayPermisos;
                    break; 
                    // case 'vueencuesta':
                    // salida=require('./components/Encuesta.vue').default.data().arrayPermisos;
                    // break; 
                    case 'envioemail':
                    salida=require('./components/EnvioEmail.vue').default.data().arrayPermisos;
                    break;  
                    case 'estudiante':
                    salida=require('./components/Estudiante.vue').default.data().arrayPermisos;
                    break; 
                    case 'paralelos':
                    salida=require('./components/Paralelo.vue').default.data().arrayPermisos;
                    break; 
                    case 'promocion':
                    salida=require('./components/Promocion.vue').default.data().arrayPermisos;
                    break; 
                    case 'unidadacademica':
                    salida=require('./components/UnidadAcademica.vue').default.data().arrayPermisos;
                    break; 
                    case 'carreras':
                    salida=require('./components/Carreras.vue').default.data().arrayPermisos;
                    break; 
            }
            return salida;
        },
        openmodal(){
            this.$root.$emit('openmodalupdate');
        },
        nologin: function() { 
            this.$root.$emit('nologin');
            swal({
                title: 'Su sesion a caducado',
                text: 'Debe de ingresar al sistema nuevamente',
                type: 'error',
                footer: 'Al ir a inicio, sus datos registrados en el formulario previo no seran recuperados.',
                showCancelButton: true,
                allowOutsideClick: false,
                allowEscapeKey:false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#89898a',
                confirmButtonText: 'Ir a inicio',
                cancelButtonText: 'Seguir',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger' 
                }).then((result) => {
                    if (result.value) {
                        window.location.href = "/";
                    }  
                }); 
        },init(){
            $('.login').on('submit', function(e) {
                e.preventDefault();
                if (this.working) return;
                this.working = true;
                var $this = $(this),
                $state = $this.find('button > .state');
                $this.addClass('loading');
                $state.html('Verificando datos'); 
                setTimeout(function() {
                    e.currentTarget.submit();
                  }, 2000);
              });
        }
    },
    mounted(){
       this.init();
    }
});
