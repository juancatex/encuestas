@extends('principal.principal')
    @section('contenido')
    <template v-if="menu==0">
                <home></home>
            </template>
            <template v-if="menu==20">
                <vueencuesta></vueencuesta>
            </template>
            <template v-if="menu==21">
                <envioemail></envioemail>
            </template>
            <template v-if="menu==22">
                <resultados></resultados>
            </template>
 
        
    @endsection
    @section('cabesera')
    <div class="navbar-wrapper">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
          <li class="nav-item">
            <a class="navbar-brand" href="/">
              <img class="brand-logo" alt="UEFAB" src="app-assets/images/logo/stack-logo-light.png">
              <h2 class="brand-text">UEFAB</h2>
            </a>
          </li>
          <li class="nav-item d-md-none">
            <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a>
          </li>
        </ul>
      </div>
      <div class="navbar-container content">
        <div class="collapse navbar-collapse" id="navbar-mobile">
          <ul class="nav navbar-nav mr-auto float-left">
            <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>   
          </ul>
          <ul class="nav navbar-nav float-right">
             
            <li class="dropdown dropdown-user nav-item">
              <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                <span class="avatar avatar-online">
                  <img src="img/avatars/{{ Auth::user()->logo }}.jpg" alt="avatar"><i></i></span>
                <span class="user-name">{{ Auth::user()->usuario }}</span>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" @click="openmodal()"><i class="ft-settings"></i>Configuraciones</a>
                
                <div class="dropdown-divider"></div> 
                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="ft-power"></i> {{ __('Logout') }}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
              
            </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    @endsection
    @section('menus')
    <?php 
        use App\Models\GrupoMenus; 
        use App\Models\Rol; 
        use App\Models\RolVues; 
          $grupos = GrupoMenus::where('activo',1)->orderBy('idgrupo', 'asc')->get();  
          $rol = Rol::where('id',Auth::user()->idrol)->first();   
        ?>
    <div class="main-menu-content">
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class=" navigation-header">
          <span><?php echo $rol->nombre; ?></span><i class=" ft-minus" data-toggle="tooltip" data-placement="right"
          data-original-title="General"></i>
        </li>
        <li class=" nav-item"><a @click="menu='home'"><i class="ft-airplay"></i><span class="menu-title" data-i18n="">Escritorio</span></a>
        </li>
        
        <?php
            foreach ($grupos as $grupo) {
              if(count($grupo->vuess)>0){
                echo '<li class=" nav-item"><a><i class="'.$grupo->icono.'"></i><span class="menu-title" data-i18n="">'.$grupo->descripcion.'</span><span class="badge badge badge-primary badge-pill float-right mr-2">'.count($grupo->vuess).'</span></a>
                 <ul class="menu-content">  '; 
                 foreach($grupo->vuess as $vue ) {
                  $permisos = RolVues::where('idrol',Auth::user()->idrol)->where('idvue',$vue->idvue)->first(); 
                    echo '<li><a class="menu-item" @click="menu=\''.$vue->template.'\';perm=\''.$permisos->permisos.'\';" >'.$vue->nombre.'</a></li>'; 
                    
                 }
              echo '</ul></li>';
              } 
            } 
        ?> 
      </ul>
    </div>
    @endsection
    @section('data')
    <!-- <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0">Fixed Navbar &amp; Footer</h3> 
        </div> 
      </div>
            <div class="content-body">

            </div>
    </div> -->
          
                <modaluser></modaluser>
            
            <template >
              <component  v-bind:is="menu" ip="{{ config('app.servidoremails', 'https://eufab.website') }}" :permisos="perm"></component>  
            </template>
    @endsection

