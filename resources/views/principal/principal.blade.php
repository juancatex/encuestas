<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <meta name="csrf-token" content="{{ csrf_token() }}"> 
    <title>{{ config('app.name', 'UEFAB') }}</title>  
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"  rel="stylesheet">
 
  <link rel="stylesheet" type="text/css" href="app-assets/css/vendors.css">
  <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/ui/prism.min.css"> 
  <link rel="stylesheet" type="text/css" href="app-assets/css/app.css"> 
  <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.css">
  <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css"> 
  <link rel="stylesheet" type="text/css" href="assets/css/style.css">

</head>

<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar"
data-open="click" data-menu="vertical-menu" data-col="2-columns">
  <div id="app">
   
                <nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-dark bg-gradient-x-primary navbar-shadow">
                    @yield('cabesera') 
                </nav> 
                <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
                    @yield('menus') 
                </div>
                <div class="app-content content">
                    @yield('data') 
                </div>
  <footer class="footer fixed-bottom footer-dark navbar-border">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2021</span>
      <span class="float-md-right d-block d-md-inline-block d-none d-lg-block">Desarrollado por :  <strong>Maria Jimena Tarqui Flores</strong></span>
    </p>
  </footer>
</div>
    <script src="js/app.js"></script> 
  <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script> 
  <script src="app-assets/vendors/js/ui/prism.min.js" type="text/javascript"></script> 
  <script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
  <script src="app-assets/js/core/app.js" type="text/javascript"></script>
  <script src="app-assets/js/scripts/customizer.js" type="text/javascript"></script>
  <script src="app-assets/js/scripts/sweetalert2.all.js" type="text/javascript"></script>
  <script src="app-assets/vendors/js/forms/tags/form-field.js" type="text/javascript"></script>
</body>
</html>