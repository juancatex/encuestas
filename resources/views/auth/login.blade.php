@extends('layouts.login')

@section('content') 

<form class="login" method="POST" action="{{ route('login') }}" style="text-align: center;">
            @csrf
                @if ($errors->any())
                    <div   style="border-radius: 11px;
                        margin: 0px;
                        background-color: #ff000091;
                        padding: 14px;
                        color: white;
                        margin-bottom: 44px;" >
                        @foreach ($errors->all() as $error)
                        <p style="margin: 0;padding: 0;">{{ $error }}</p>
                        @endforeach
                    </div>
                @endif 
                <!-- <p class="title">{{ config('app.name', 'Laravel') }}</p> -->
                <img src="img/logoin.jpg" alt="UEFAB">
                <p class="title">U E F A B</p>
                <input type="text" placeholder="Usuario" name="usuario" autofocus/>
                <i class="fa fa-user"></i>
                <input type="password" placeholder="Contraseña" name="password"/>
                <i class="fa fa-key"></i>
               
                <button>
                <i class="spinner"></i>
                <span class="state">Ingresar</span>
                </button>
            </form>
@endsection
