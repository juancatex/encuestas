<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { 
    if(Auth::check()) 
    return view('welcome');
     else  
    return redirect()->route('login');
}); 
Route::get('/getexcel', function () {
    return Storage::download('form.xlsx');
  })->name('getexcel');
Route::get('/getexceles', function () {
    return Storage::download('form_estudiantes.xlsx');
  })->name('getexceles');
Auth::routes();
Route::get('/encuesta', [App\Http\Controllers\EncuestaController::class, 'openencuesta']);
Route::get('/getencuesta', [App\Http\Controllers\EncuestaController::class, 'getencuesta']);
Route::post('/encuestafinal/registrar', [App\Http\Controllers\RespuestasController::class, 'registro']);
Route::get('/reporteEstudiante', [App\Http\Controllers\ReportesController::class, 'estuReport'])->name('reporteEstudiante');
Route::get('/reporteUniversidad', [App\Http\Controllers\ReportesController::class, 'univerReport']);
Route::get('/reporteCarrera', [App\Http\Controllers\CarreraController::class, 'carreraReporte'])->name('reporteCarrera');
Route::get('/encuestasReporte', [App\Http\Controllers\EncuestaController::class, 'encuestasReporte']);

Route::middleware('auth')->group(function () {
    Route::get('/encuestasReportemain', [App\Http\Controllers\EncuestaController::class, 'encuestasReportemain']);
    Route::get('/listaencuesta', [App\Http\Controllers\EncuestaController::class, 'lista']);
    Route::get('/listaencuestaresultado', [App\Http\Controllers\EncuestaController::class, 'listaresultados']); 
    Route::post('/encuesta/registrar', [App\Http\Controllers\EncuestaController::class, 'registro']); 
    Route::post('/encuesta/registrarexcel', [App\Http\Controllers\EncuestaController::class, 'registrarexcel']); 
    Route::post('/encuesta/update', [App\Http\Controllers\EncuestaController::class, 'updata']); 
    Route::post('/encuesta/delete', [App\Http\Controllers\EncuestaController::class, 'delete']); 
    Route::post('/respuesta/registrar', [App\Http\Controllers\EncuestaDataController::class, 'registro']);
    Route::post('/respuesta/updata', [App\Http\Controllers\EncuestaDataController::class, 'updata']);
    Route::post('/respuesta/delete', [App\Http\Controllers\EncuestaDataController::class, 'delete']); 
    Route::get('/promocion/selectPromocionemail', [App\Http\Controllers\EncuestaController::class, 'selectPromocion']); 
    Route::post('/sendemail', [App\Http\Controllers\EncuestaController::class, 'sendemail']); 
    Route::post('/pregunta/registrar', [App\Http\Controllers\EncuestaPreguntasController::class, 'registro']);
    Route::post('/pregunta/updata', [App\Http\Controllers\EncuestaPreguntasController::class, 'updata']);
    Route::post('/pregunta/delete', [App\Http\Controllers\EncuestaPreguntasController::class, 'delete']);
    Route::get('/encuestararesultadoid', [App\Http\Controllers\RespuestasController::class, 'getencuestaresultado']);   

    Route::get('/getgrupos', [App\Http\Controllers\GrupoMenusController::class, 'getgrupos']);   
    Route::get('/estudiantePromocion', [App\Http\Controllers\PersonalPromocionController::class, 'index']);
    Route::get('/estudiantePromocion2', [App\Http\Controllers\PersonalPromocionController::class, 'index2']);
    Route::get('/promocion/selectPromocion',[App\Http\Controllers\PromocionController::class, 'selectPromocion']);
    Route::get('/paralelo/selectParalelo',  [App\Http\Controllers\ParaleloController::class, 'selectParalelo']);
    Route::post('/estudiantePromocion/registrar', [App\Http\Controllers\PersonalPromocionController::class, 'store']);
    Route::post('/estudiantePromocion/registrarexcel', [App\Http\Controllers\PersonalPromocionController::class, 'registrarexcel']);
    Route::put('/estudiantePromocion/actualizar', [App\Http\Controllers\PersonalPromocionController::class, 'update']);
    
    Route::put('/estudiantePromocion/desactivar', [App\Http\Controllers\PersonalPromocionController::class, 'desactivar']);
    Route::put('/estudiantePromocion/activar', [App\Http\Controllers\PersonalPromocionController::class, 'activar']);

    Route::get('/paralelo', [App\Http\Controllers\ParaleloController::class, 'index']);
    Route::post('/paralelo/registrar', [App\Http\Controllers\ParaleloController::class, 'store']);

    Route::put('/paralelo/actualizar', [App\Http\Controllers\ParaleloController::class, 'update']);
    Route::put('/paralelo/desactivar', [App\Http\Controllers\ParaleloController::class, 'desactivar']);
    Route::put('/paralelo/activar', [App\Http\Controllers\ParaleloController::class, 'activar']);
  
    Route::get('/promocion', [App\Http\Controllers\PromocionController::class, 'index']);
    Route::post('/promocion/registrar', [App\Http\Controllers\PromocionController::class, 'store']);
    Route::put('/promocion/actualizar', [App\Http\Controllers\PromocionController::class, 'update']);
    Route::put('/promocion/desactivar', [App\Http\Controllers\PromocionController::class, 'desactivar']);
    Route::put('/promocion/activar', [App\Http\Controllers\PromocionController::class, 'activar']);

    Route::get('/instituto', [App\Http\Controllers\InstitutoController::class, 'index']); 
    Route::post('/instituto/registrar', [App\Http\Controllers\InstitutoController::class, 'store']);
    Route::put('/instituto/actualizar', [App\Http\Controllers\InstitutoController::class, 'update']);
    Route::put('/instituto/desactivar', [App\Http\Controllers\InstitutoController::class, 'desactivar']);
    Route::put('/instituto/activar', [App\Http\Controllers\InstitutoController::class, 'activar']);
    Route::get('/instituto/selectInstituto', [App\Http\Controllers\InstitutoController::class, 'selectInstituto']);


    Route::get('/carrera',[App\Http\Controllers\CarreraController::class, 'index']);
    Route::get('/getcarreras',[App\Http\Controllers\CarreraController::class, 'getcarreras']);
    Route::post('/carrera/registrar', [App\Http\Controllers\CarreraController::class, 'store']);
    Route::put('/carrera/actualizar', [App\Http\Controllers\CarreraController::class, 'update']);
    Route::put('/carrera/desactivar', [App\Http\Controllers\CarreraController::class, 'desactivar']);
    Route::put('/carrera/activar', [App\Http\Controllers\CarreraController::class, 'activar']);
    Route::get('/carrera/selectCategoria', [App\Http\Controllers\CarreraController::class, 'selectCategoria']); 
    Route::get('/carrera/selectBuscarCarrera', [App\Http\Controllers\CarreraController::class, 'selectbuscarCarrera']);

    Route::get('/user', [App\Http\Controllers\UserController::class, 'index']);
    Route::get('/getuserupdate', [App\Http\Controllers\UserController::class, 'getuserupdate']);
    Route::post('/user/registrar',  [App\Http\Controllers\UserController::class, 'store']);
    Route::put('/user/actualizar', [App\Http\Controllers\UserController::class, 'update']);
    Route::put('/user/desactivar', [App\Http\Controllers\UserController::class, 'desactivar']);
    Route::put('/user/activar', [App\Http\Controllers\UserController::class, 'activar']);

    Route::get('/rol', [App\Http\Controllers\RolController::class, 'index']);
    Route::get('/rolasig', [App\Http\Controllers\RolController::class, 'rolasig']);
    Route::get('/rol/selectRol', [App\Http\Controllers\RolController::class, 'selectRol']);

    Route::put('/rol/actualizar', [App\Http\Controllers\RolController::class, 'update']);
    Route::post('/rol/registrar',  [App\Http\Controllers\RolController::class, 'store']);
    Route::put('/rol/desactivar', [App\Http\Controllers\RolController::class, 'desactivar']);
    Route::put('/rol/activar', [App\Http\Controllers\RolController::class, 'activar']);

    Route::get('/getvuesrol', [App\Http\Controllers\RolVuesController::class, 'getvuesrol']);
    Route::post('/regrolvue', [App\Http\Controllers\RolVuesController::class, 'regrolvue']);
    Route::put('/regrolvue/desactivar', [App\Http\Controllers\RolVuesController::class, 'desactivar']);
    Route::put('/regrolvue/activar', [App\Http\Controllers\RolVuesController::class, 'activar']);

    Route::get('/getvues', [App\Http\Controllers\VuesController::class, 'getvues']);
});