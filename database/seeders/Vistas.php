<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Vistas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vues')->insert(['idgrupo'=>1,'nombre'=>'Usuarios','template'=>'users','orden'=>1]);   
        DB::table('vues')->insert(['idgrupo'=>1,'nombre'=>'Rol','template'=>'rol','orden'=>2]);   
        DB::table('vues')->insert(['idgrupo'=>1,'nombre'=>'Asignación de Vistas','template'=>'rolvues','orden'=>3]);   
        DB::table('vues')->insert(['idgrupo'=>2,'nombre'=>'Encuesta','template'=>'vueencuesta','orden'=>1]);   
        DB::table('vues')->insert(['idgrupo'=>2,'nombre'=>'Envio de encuesta','template'=>'envioemail','orden'=>2]);   
        DB::table('vues')->insert(['idgrupo'=>2,'nombre'=>'Resultados','template'=>'resultados','orden'=>3]);   
        DB::table('vues')->insert(['idgrupo'=>3,'nombre'=>'Estudiante','template'=>'estudiante','orden'=>1]);   
        DB::table('vues')->insert(['idgrupo'=>3,'nombre'=>'Paralelo','template'=>'paralelos','orden'=>2]);   
        DB::table('vues')->insert(['idgrupo'=>3,'nombre'=>'Promocion','template'=>'promocion','orden'=>3]);   
        DB::table('vues')->insert(['idgrupo'=>4,'nombre'=>'Estudios','template'=>'estudios','orden'=>1]); 
        DB::table('vues')->insert(['idgrupo'=>4,'nombre'=>'Unidad Academica','template'=>'unidadacademica','orden'=>2]); 
        DB::table('vues')->insert(['idgrupo'=>4,'nombre'=>'Carreras','template'=>'carreras','orden'=>3]); 
    }
}
