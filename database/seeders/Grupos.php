<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Grupos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grupo_menuses')->insert(['idgrupo'=>1,'descripcion'=>'Administración','icono'=>'ft-grid']);   
        DB::table('grupo_menuses')->insert(['idgrupo'=>2,'descripcion'=>'Encuesta','icono'=>'ft-check-square']);   
        DB::table('grupo_menuses')->insert(['idgrupo'=>3,'descripcion'=>'Unidad','icono'=>'ft-users']);   
        DB::table('grupo_menuses')->insert(['idgrupo'=>4,'descripcion'=>'Academico','icono'=>'ft-tag']);   
    }
}
