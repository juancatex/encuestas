<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Rolvues extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('rol_vues')->insert(['idrol'=>1,'idvue'=>1]);   
        // DB::table('rol_vues')->insert(['idrol'=>1,'idvue'=>2]);   
        DB::table('rol_vues')->insert(['idrol'=>1,'idvue'=>3,'permisos'=>'%7B%22Asignar%22:true,%22Activar%22:true,%22Desactivar%22:true%7D']);   
        // DB::table('rol_vues')->insert(['idrol'=>1,'idvue'=>4]);   
        // DB::table('rol_vues')->insert(['idrol'=>1,'idvue'=>5]);   
        // DB::table('rol_vues')->insert(['idrol'=>1,'idvue'=>6]);   
        // DB::table('rol_vues')->insert(['idrol'=>1,'idvue'=>7]);   
        // DB::table('rol_vues')->insert(['idrol'=>1,'idvue'=>8]);   
        // DB::table('rol_vues')->insert(['idrol'=>1,'idvue'=>9]);   
        // DB::table('rol_vues')->insert(['idrol'=>1,'idvue'=>10]);   
        // DB::table('rol_vues')->insert(['idrol'=>1,'idvue'=>11]);   
        // DB::table('rol_vues')->insert(['idrol'=>1,'idvue'=>12]);   
    }
}
