<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vues', function (Blueprint $table) {
            $table->increments('idvue');  
            $table->integer('idgrupo')->unsigned();
            $table->string('nombre')->nullable(); 
            $table->string('template')->nullable(); 
            $table->boolean('activo')->default(1);  
            $table->integer('orden');
            $table->timestamps();
            $table->foreign('idgrupo')->references('idgrupo')->on('grupo_menuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vues');
    }
}
