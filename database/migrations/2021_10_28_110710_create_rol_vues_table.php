<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolVuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rol_vues', function (Blueprint $table) {
            $table->increments('id');  
            $table->integer('idrol')->unsigned();
            $table->integer('idvue')->unsigned();
            $table->string('permisos')->default('{}');
            $table->boolean('activo')->default(1);
            $table->timestamps();
            $table->foreign('idrol')->references('id')->on('roles');
            $table->foreign('idvue')->references('idvue')->on('vues');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rol_vues');
    }
}
